# import image
# import PIL.Image
# from pytesseract import image_to_string

import io
import os
# Imports the Google Cloud client library
from google.cloud import vision
from google.cloud.vision import types

def detect_text(path):
    """Detects text in the file."""
    client = vision.ImageAnnotatorClient()

    with io.open(path, 'rb') as image_file:
        content = image_file.read()

    image = types.Image(content=content)

    response = client.text_detection(image=image)
    texts = response.text_annotations
    print('Texts:')

    for text in texts:
        print('\n"{}"'.format(text.description))

        vertices = (['({},{})'.format(vertex.x, vertex.y)
                    for vertex in text.bounding_poly.vertices])

        print('bounds: {}'.format(','.join(vertices)))


#"""Returns document bounds given an image."""
#client = vision.ImageAnnotatorClient()

path = 'C:/Users/Mayur Sonawane/Documents/meeting_images/Banner sito WAIMH.jpg'
with io.open('C:/Users/Mayur Sonawane/Documents/meeting_images/Banner sito WAIMH.jpg', 'rb') as image_file:
    content = image_file.read()

image = types.Image(content=content)

response = client.document_text_detection(image=image)
document = response.full_text_annotation
print(document)
#print image_to_string(Image.open('test-english.jpg'), lang='eng')