from PIL import Image
import cv2
import pytesseract
import numpy as np

pytesseract.pytesseract.tesseract_cmd = 'C:/Program Files/Tesseract-OCR/tesseract.exe'
file_name = 'C:/Users/Mayur Sonawane/PycharmProjects/meetinginfo_extraction/Images/Banner sito WAIMH.jpg'
file_name = 'C:/Users/Mayur Sonawane/PycharmProjects/meetinginfo_extraction/Images/early-registration-extension.jpg'
file_name = 'C:/Users/Mayur Sonawane/PycharmProjects/meetinginfo_extraction/Images/BMS.png'
#file_name = 'C:/Users/Mayur Sonawane/PycharmProjects/meetinginfo_extraction/Images/16.jpg'
#im = Image.open(file_name)

oriimage = cv2.imread(file_name)
print('Original Image size- ',oriimage.shape)

#increasing the size of images, so that small characters can get detected
size = 4
newx,newy = oriimage.shape[1]*size,oriimage.shape[0]*size #new size (w,h)
newimage = cv2.resize(oriimage,(newx,newy))
print('Expanded Image size- ',newimage.shape)

img = newimage

# Apply dilation and erosion to remove some noise
kernel = np.ones((1, 1), np.uint8)
img = cv2.dilate(img, kernel, iterations=50)
img = cv2.erode(img, kernel, iterations=40)
cv2.imshow('Opening Operation', img)
cv2.waitKey()

img2gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
cv2.imshow('Gray Image', img2gray)
cv2.waitKey()
ret, mask = cv2.threshold(img2gray, 180, 255, cv2.THRESH_BINARY)
#print(len(mask))


image_mask = cv2.bitwise_and(img2gray, img2gray, mask=mask)
cv2.imshow('After Masking', image_mask)
cv2.waitKey()


# Apply dilation and erosion to remove some noise
kernel = np.ones((1, 1), np.uint8)
img = cv2.dilate(image_mask, kernel, iterations=100)
image_final = cv2.erode(img, kernel, iterations=5)
cv2.imshow('Closing Operation', img)
cv2.waitKey()

text = pytesseract.image_to_string(image_final, lang = 'eng')
print(text)
print('___________________________')
text = text.encode('utf8')
print(text)
