from base64 import b64encode
from os import makedirs,listdir
from os.path import join, basename
import cv2
from sys import argv
import json
import requests
import keys ##keys contain the input images path and api key

ENDPOINT_URL = 'https://vision.googleapis.com/v1/images:annotate'
RESULTS_DIR = 'jsons'
makedirs(RESULTS_DIR, exist_ok=True)

def load_images_from_folder(folder):
    """
    load all images from the specified folder
    :param folder:
    :return: list of all images
    """

    images = []
    for filename in listdir(folder):
        img = cv2.imread(join(folder,filename))
        if img is not None:
            images.append(join(folder,filename))
    return images

def make_image_data_list(image_filenames):
    """
    :param image_filenames: is a list of filename strings
    :return: a list of dicts formatted as the Vision API
        needs them to be
    """

    img_requests = []
    for imgname in image_filenames:
        with open(imgname, 'rb') as f:
            ctxt = b64encode(f.read()).decode()
            img_requests.append({
                    'image': {'content': ctxt},
                    'features': [{
                        'type': 'TEXT_DETECTION',
                        'maxResults': 1
                    }]
            })
    return img_requests


def make_image_data(image_filenames):
    """Returns the image data lists as bytes"""
    imgdict = make_image_data_list(image_filenames)
    return json.dumps({"requests": imgdict }).encode()


def request_ocr(api_key, image_filenames):
    """Returns the response as a JSON using VISION API"""
    response = requests.post(ENDPOINT_URL,
                             data=make_image_data(image_filenames),
                             params={'key': api_key},
                             headers={'Content-Type': 'application/json'})
    return response

def detect_text(path):
    """Detects text in the file."""
    client = vision.ImageAnnotatorClient()

    with io.open(path, 'rb') as image_file:
        content = image_file.read()

    image = types.Image(content=content)

    response = client.text_detection(image=image)
    texts = response.text_annotations
    print('Texts:')

    for text in texts:
        print('\n"{}"'.format(text.description))

        vertices = (['({},{})'.format(vertex.x, vertex.y)
                    for vertex in text.bounding_poly.vertices])

        print('bounds: {}'.format(','.join(vertices)))

if __name__ == '__main__':
    #api_key, *image_filenames = argv[1:]

    api_key = keys.vision_apikey
    image_filenames = load_images_from_folder(keys.folder)
    #  = list()
    #image_filenames.append(keys.file_ip)

    if not api_key or not image_filenames:
        print("""
            Please supply an api key, then one or more image filenames

            $ python cloudvisreq.py api_key image1.jpg image2.png""")
    else:
        jpath = join(RESULTS_DIR, 'Images_Content' + '.json')
        try:
            with open(jpath, 'r') as f:
                data = json.loads(f.read())  # data becomes a dictionary
        except IOError:
            data = {}



        response = request_ocr(api_key, image_filenames)
        if response.status_code != 200 or response.json().get('error'):
            print(response.text)

        else:
            for idx, resp in enumerate(response.json()['responses']):
                # save to JSON file
                imgname = image_filenames[idx]
                imagename = basename(imgname)

                ta = resp['textAnnotations'][0]
                data[imagename] = ta['description']

            #jpath = join(RESULTS_DIR, 'Images_content' + '.json')
            with open(jpath, 'w') as f:

                datatxt = json.dumps(data ,indent=2)
                print("Wrote", len(datatxt), "bytes to", jpath)
                f.write(datatxt)

                # print the plaintext to screen for convenience
                #print("---------------------------------------------")
                #t = resp['textAnnotations'][0]
                #print("    Bounding Polygon:")
                #print(t['boundingPoly'])
                #print("    Text:")
                #print(t['description'])
